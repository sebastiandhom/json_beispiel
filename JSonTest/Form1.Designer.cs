﻿namespace JSonTest
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.pbBild = new System.Windows.Forms.PictureBox();
            this.pbBild_2 = new System.Windows.Forms.PictureBox();
            this.cmdCreate = new System.Windows.Forms.Button();
            this.cmdDeserialize = new System.Windows.Forms.Button();
            this.cmdSerialize = new System.Windows.Forms.Button();
            this.lblSerializedText = new System.Windows.Forms.TextBox();
            this.dtpDatum = new System.Windows.Forms.DateTimePicker();
            this.txtInt = new System.Windows.Forms.TextBox();
            this.txtDecimal = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtString = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.cbTypen = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.cbTypen_2 = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtString_2 = new System.Windows.Forms.TextBox();
            this.txtDecimal_2 = new System.Windows.Forms.TextBox();
            this.txtInt_2 = new System.Windows.Forms.TextBox();
            this.dtpDatum_2 = new System.Windows.Forms.DateTimePicker();
            this.cmdLoadPic = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pbBild)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbBild_2)).BeginInit();
            this.SuspendLayout();
            // 
            // pbBild
            // 
            this.pbBild.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pbBild.Location = new System.Drawing.Point(12, 70);
            this.pbBild.Name = "pbBild";
            this.pbBild.Size = new System.Drawing.Size(214, 158);
            this.pbBild.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbBild.TabIndex = 0;
            this.pbBild.TabStop = false;
            // 
            // pbBild_2
            // 
            this.pbBild_2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pbBild_2.Location = new System.Drawing.Point(905, 70);
            this.pbBild_2.Name = "pbBild_2";
            this.pbBild_2.Size = new System.Drawing.Size(214, 158);
            this.pbBild_2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbBild_2.TabIndex = 1;
            this.pbBild_2.TabStop = false;
            // 
            // cmdCreate
            // 
            this.cmdCreate.Location = new System.Drawing.Point(12, 12);
            this.cmdCreate.Name = "cmdCreate";
            this.cmdCreate.Size = new System.Drawing.Size(109, 52);
            this.cmdCreate.TabIndex = 2;
            this.cmdCreate.Text = "erstelle Objekt";
            this.cmdCreate.UseVisualStyleBackColor = true;
            this.cmdCreate.Click += new System.EventHandler(this.button1_Click);
            // 
            // cmdDeserialize
            // 
            this.cmdDeserialize.Location = new System.Drawing.Point(905, 12);
            this.cmdDeserialize.Name = "cmdDeserialize";
            this.cmdDeserialize.Size = new System.Drawing.Size(166, 52);
            this.cmdDeserialize.TabIndex = 3;
            this.cmdDeserialize.Text = "konvertiere nach json und deserialisiere zurück";
            this.cmdDeserialize.UseVisualStyleBackColor = true;
            this.cmdDeserialize.Click += new System.EventHandler(this.button2_Click);
            // 
            // cmdSerialize
            // 
            this.cmdSerialize.Location = new System.Drawing.Point(236, 12);
            this.cmdSerialize.Name = "cmdSerialize";
            this.cmdSerialize.Size = new System.Drawing.Size(99, 52);
            this.cmdSerialize.TabIndex = 4;
            this.cmdSerialize.Text = "serialisiere nach Json Text";
            this.cmdSerialize.UseVisualStyleBackColor = true;
            this.cmdSerialize.Click += new System.EventHandler(this.button3_Click);
            // 
            // lblSerializedText
            // 
            this.lblSerializedText.Location = new System.Drawing.Point(232, 70);
            this.lblSerializedText.Multiline = true;
            this.lblSerializedText.Name = "lblSerializedText";
            this.lblSerializedText.ReadOnly = true;
            this.lblSerializedText.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.lblSerializedText.Size = new System.Drawing.Size(667, 368);
            this.lblSerializedText.TabIndex = 5;
            // 
            // dtpDatum
            // 
            this.dtpDatum.Location = new System.Drawing.Point(12, 263);
            this.dtpDatum.Name = "dtpDatum";
            this.dtpDatum.Size = new System.Drawing.Size(214, 20);
            this.dtpDatum.TabIndex = 6;
            // 
            // txtInt
            // 
            this.txtInt.Location = new System.Drawing.Point(126, 289);
            this.txtInt.Name = "txtInt";
            this.txtInt.Size = new System.Drawing.Size(100, 20);
            this.txtInt.TabIndex = 7;
            // 
            // txtDecimal
            // 
            this.txtDecimal.Location = new System.Drawing.Point(126, 315);
            this.txtDecimal.Name = "txtDecimal";
            this.txtDecimal.Size = new System.Drawing.Size(100, 20);
            this.txtDecimal.TabIndex = 7;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(86, 292);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(28, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Zahl";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(57, 318);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Dezimalzahl";
            // 
            // txtString
            // 
            this.txtString.Location = new System.Drawing.Point(126, 341);
            this.txtString.Name = "txtString";
            this.txtString.Size = new System.Drawing.Size(100, 20);
            this.txtString.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(86, 344);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(34, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "String";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // cbTypen
            // 
            this.cbTypen.FormattingEnabled = true;
            this.cbTypen.Location = new System.Drawing.Point(126, 367);
            this.cbTypen.Name = "cbTypen";
            this.cbTypen.Size = new System.Drawing.Size(100, 21);
            this.cbTypen.TabIndex = 9;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(87, 370);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(25, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Typ";
            this.label5.Click += new System.EventHandler(this.label4_Click);
            // 
            // cbTypen_2
            // 
            this.cbTypen_2.Enabled = false;
            this.cbTypen_2.FormattingEnabled = true;
            this.cbTypen_2.Location = new System.Drawing.Point(1019, 338);
            this.cbTypen_2.Name = "cbTypen_2";
            this.cbTypen_2.Size = new System.Drawing.Size(100, 21);
            this.cbTypen_2.TabIndex = 18;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(980, 341);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(25, 13);
            this.label1.TabIndex = 14;
            this.label1.Text = "Typ";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(979, 315);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(34, 13);
            this.label6.TabIndex = 15;
            this.label6.Text = "String";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(950, 289);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(63, 13);
            this.label7.TabIndex = 16;
            this.label7.Text = "Dezimalzahl";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(979, 263);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(28, 13);
            this.label8.TabIndex = 17;
            this.label8.Text = "Zahl";
            // 
            // txtString_2
            // 
            this.txtString_2.Enabled = false;
            this.txtString_2.Location = new System.Drawing.Point(1019, 312);
            this.txtString_2.Name = "txtString_2";
            this.txtString_2.ReadOnly = true;
            this.txtString_2.Size = new System.Drawing.Size(100, 20);
            this.txtString_2.TabIndex = 11;
            // 
            // txtDecimal_2
            // 
            this.txtDecimal_2.Enabled = false;
            this.txtDecimal_2.Location = new System.Drawing.Point(1019, 286);
            this.txtDecimal_2.Name = "txtDecimal_2";
            this.txtDecimal_2.ReadOnly = true;
            this.txtDecimal_2.Size = new System.Drawing.Size(100, 20);
            this.txtDecimal_2.TabIndex = 12;
            // 
            // txtInt_2
            // 
            this.txtInt_2.Enabled = false;
            this.txtInt_2.Location = new System.Drawing.Point(1019, 260);
            this.txtInt_2.Name = "txtInt_2";
            this.txtInt_2.ReadOnly = true;
            this.txtInt_2.Size = new System.Drawing.Size(100, 20);
            this.txtInt_2.TabIndex = 13;
            // 
            // dtpDatum_2
            // 
            this.dtpDatum_2.Enabled = false;
            this.dtpDatum_2.Location = new System.Drawing.Point(905, 234);
            this.dtpDatum_2.Name = "dtpDatum_2";
            this.dtpDatum_2.Size = new System.Drawing.Size(214, 20);
            this.dtpDatum_2.TabIndex = 10;
            // 
            // cmdLoadPic
            // 
            this.cmdLoadPic.Location = new System.Drawing.Point(12, 234);
            this.cmdLoadPic.Name = "cmdLoadPic";
            this.cmdLoadPic.Size = new System.Drawing.Size(214, 23);
            this.cmdLoadPic.TabIndex = 19;
            this.cmdLoadPic.Text = "lade eigenes Bild";
            this.cmdLoadPic.UseVisualStyleBackColor = true;
            this.cmdLoadPic.Click += new System.EventHandler(this.cmdLoadPic_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1129, 450);
            this.Controls.Add(this.cmdLoadPic);
            this.Controls.Add(this.cbTypen_2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txtString_2);
            this.Controls.Add(this.txtDecimal_2);
            this.Controls.Add(this.txtInt_2);
            this.Controls.Add(this.dtpDatum_2);
            this.Controls.Add(this.cbTypen);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtString);
            this.Controls.Add(this.txtDecimal);
            this.Controls.Add(this.txtInt);
            this.Controls.Add(this.dtpDatum);
            this.Controls.Add(this.lblSerializedText);
            this.Controls.Add(this.cmdSerialize);
            this.Controls.Add(this.cmdDeserialize);
            this.Controls.Add(this.cmdCreate);
            this.Controls.Add(this.pbBild_2);
            this.Controls.Add(this.pbBild);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pbBild)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbBild_2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pbBild;
        private System.Windows.Forms.PictureBox pbBild_2;
        private System.Windows.Forms.Button cmdCreate;
        private System.Windows.Forms.Button cmdDeserialize;
        private System.Windows.Forms.Button cmdSerialize;
        private System.Windows.Forms.TextBox lblSerializedText;
        private System.Windows.Forms.DateTimePicker dtpDatum;
        private System.Windows.Forms.TextBox txtInt;
        private System.Windows.Forms.TextBox txtDecimal;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtString;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cbTypen;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cbTypen_2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtString_2;
        private System.Windows.Forms.TextBox txtDecimal_2;
        private System.Windows.Forms.TextBox txtInt_2;
        private System.Windows.Forms.DateTimePicker dtpDatum_2;
        private System.Windows.Forms.Button cmdLoadPic;
    }
}

