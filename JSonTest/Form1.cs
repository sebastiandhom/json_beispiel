﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using Newtonsoft.Json;

namespace JSonTest
{
    public partial class Form1 : Form
    {
        private meinObjekt o = null;

        private List<meinObjektTyp> objekttypen = new List<meinObjektTyp> {
                                                    new meinObjektTyp { Name = "Typ A" },
                                                    new meinObjektTyp { Name = "Typ B" }
                                                  };

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            o = new meinObjekt
            {
                Datum = DateTime.Now,
                Dezimalzahl = (decimal)2.304,
                Text = "Name",
                Zahl = 1,
                Typ = objekttypen[0],
                bild = new Bilddaten { Foto = Properties.Resources.katze }
            };

            txtDecimal.Text = o.Dezimalzahl.ToString();
            txtInt.Text = o.Zahl.ToString();
            dtpDatum.Value = o.Datum;
            cbTypen.SelectedText = o.Typ.Name;
            txtString.Text = o.Text;
            pbBild.Image = o.bild.Foto;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var o = JsonConvert.DeserializeObject<meinObjekt>(lblSerializedText.Text);

            txtDecimal_2.Text = o.Dezimalzahl.ToString();
            txtInt_2.Text = o.Zahl.ToString();
            dtpDatum_2.Value = o.Datum;
            cbTypen_2.Text = o.Typ.Name;
            txtString_2.Text = o.Text;
            pbBild_2.Image = o.bild.Foto;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            o.Datum = dtpDatum.Value;
            o.Text = txtString.Text;
            int tmp = 0;
            int.TryParse(txtInt.Text, out tmp);
            o.Zahl = tmp;
            decimal tmpDec = 0;
            decimal.TryParse(txtDecimal.Text, out tmpDec);
            o.Dezimalzahl = tmpDec;
            o.bild.Foto = pbBild.Image;
            o.Typ.Name = cbTypen.Text;

            lblSerializedText.Text = JsonConvert.SerializeObject(o);
        }

        private void label4_Click(object sender, EventArgs e)
        {
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            foreach (var typ in objekttypen)
            {
                cbTypen.Items.Add(typ.Name);
                cbTypen_2.Items.Add(typ.Name);
            }
        }

        private void cmdLoadPic_Click(object sender, EventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Filter = "JPG-Dateien|*.jpg";
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                pbBild.Image = Image.FromFile(dlg.FileName);
            }
        }
    }

    /// <summary>
    /// eine Klasse für ein Objekt, das serialisiert werden soll
    /// </summary>
    internal class meinObjekt
    {
        //normale Datentypen
        public int Zahl { get; set; }

        public string Text { get; set; }
        public DateTime Datum { get; set; }
        public decimal Dezimalzahl { get; set; }

        //komplexe Datentypen
        public meinObjektTyp Typ { get; set; }

        public Bilddaten bild { get; set; }
    }

    /// <summary>
    /// untergeordneter komplexer Datentyp mit normalen Feld-Datentypen
    /// </summary>
    internal class meinObjektTyp
    {
        public string Name { get; set; }
    }

    /// <summary>
    /// untergeordneter komplexer Datentyp mit komplexen Feld-Datentypen (Binärdaten)
    /// </summary>
    internal class Bilddaten
    {
        //ohne dieses Attribut würde die Klasse nicht serialisiert werden können, da beim späteren Deserialisieren ein String nicht in den Image-Typen konvertiert werden kann.
        [JsonConverter(typeof(ImageToJsonConverter))]
        public Image Foto { get; set; }
    }

    /// <summary>
    /// Dafür gibt es diese Klasse, die einen JsonConverter implementiert
    /// </summary>
    internal class ImageToJsonConverter : JsonConverter
    {
        /// <summary>
        /// das übergebenen Objekt (welches vom Typ Image sein muss)
        /// wird in ein serialisierbares Byte[] konvertiert
        /// </summary>
        /// <param name="writer"></param>
        /// <param name="value"></param>
        /// <param name="serializer"></param>
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            var image = (Image)value;
            // save to memory stream in original format
            var ms = new MemoryStream();
            image.Save(ms, image.RawFormat);
            byte[] imageBytes = ms.ToArray();
            // write byte array, will be converted to base64 by JSON.NET
            writer.WriteValue(imageBytes);
        }

        /// <summary>
        /// das im Json befindliche Byte[] kann wieder in ein Image zurückverwandelt werden
        /// </summary>
        /// <param name="reader"></param>
        /// <param name="objectType"></param>
        /// <param name="existingValue"></param>
        /// <param name="serializer"></param>
        /// <returns></returns>
        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            var base64 = (string)reader.Value;
            // convert base64 to byte array, put that into memory stream and feed to image
            return Image.FromStream(new MemoryStream(Convert.FromBase64String(base64)));
        }

        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(Image);
        }
    }
}